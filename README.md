Bildirge.org
===================
[Bildirge.org](https://bildirge.org): Aşağıda da bulunan[<sup>bildirge</sup>](#bildirge) "Kullanıcı Hakları Bildirgesi"nin yayınlanması amacıyla oluşturulmuş bir web sayfasıdır. Bu depo mevcut web sitesine ait dosyaları barındırmaktadır.  

####Sertifika Bilgileri:

SHA-256 Fingerprint:
20:8C:8C:AE:4F:52:1C:3B:A2:9C:DA:4C:BD:5B:BA:B9:CC:7B:E2:B8:55:DC:77:F0:FF:01:9C:E6:BD:64:E6:1A

SHA1 Fingerprint:<br> 
F4:C9:1C:3E:DF:B9:04:B4:04:55:31:A6:74:7F:F8:82:7B:58:6F:67

---
<a id="bildirge"></a>

![](https://raw.githubusercontent.com/AlternatifBilisim/bildirge.org/master/images/community.png)   Kullanıcı Hakları Bildirgesi
===

+ İnternet'e erişim temel bir **haktır**.

+ Devlet gerekli yasal düzenlemeler ve icra organlarıyla bu hakkı güvence altına almakla yükümlüdür.

+ İnternet'e erişim için gerekli olan altyapı teknolojilerinden, mümkünse **ücretsiz ya da olabilecek en az** bedeller karşılığında faydalanabilmek herkesin hakkıdır.

+ Ulusal/fiziki sınırların olmadığı, evrensel bir ortam olan internette kullanıcılar, hiç bir sınıfsal, ulusal, kültürel, cinsel, dinsel vb. ayrımlar gözetilmeksizin **eşittirler**.

+ İnternet'in etkin bir biçimde kullanılabilmesi için gerekli güncel bilgileri temel eğitim sisteminin bir parçası haline getirmek bir kamu sorumluluğudur.

+ İnternet için kurulan altyapılar **şeffaf** olmalıdır. Bu sistemler için kullanılan donanımsal/yazılımsal teknolojiler ile bu yapıları kuran/işleten, özel/kamusal kuruluşlar kullanıcıların denetimlerine açık olmalıdır. Şeffaflık yurttaşın temel hakkı, kamusal düzenleyici ve hizmet sağlayıcıların ödevidir.

+ Kullanıcıların seçimlerine saygı, İnternetin hem sosyal açıdan hem de teknik açıdan özgürce gelişebilmesi için **ağ tarafsızlığı**, altyapı ve hizmet sağlayıcılar için zorunlu bir ilkedir. *Ağ tarafsızlığı, altyapı ve hizmet sağlayıcılarının farklı içerik ve uygulamalar arasında ayrımcılık yapamayacağı anlamına gelir. Ayrıca kullanıcının her ekipmanı, içeriği ve hizmeti, hizmet sağlayıcının herhangi bir müdahalesi olmadan kullanabilmesini mümkün kılar.* Tarafsız internet erişimi her kullanıcının hakkıdır.

+ İnternet bugün, düşünce ve ifade özgürlüğünün gerçekleştiği öncelikli iletişim alanı haline gelmiştir; aynı şekilde, müdahale edilmeden, sansürlenmeden bilgi edinme ve haber alma hakkının özgürce kullanılabildiği en önemli platformdur. Dahası, internet herkesi bir yayıncı haline getirmekte, bu yönüyle iletişimi demokratikleştirmekte ve kamu yararının ortaya çıktığı ayrıcalıklı iletişim ve etkileşim platformuna dönüşmektedir. İşte bu yüzden, internetin **evrenselliği, bütünlüğü, açıklığı ve çok sesliliği** korunmalıdır.

+ İnternet insani etkileşim ve sosyal ilişki için temel bir platform haline gelmiştir. Bu durum, internet erişimini en az seyahat özgürlüğü kadar temel bir insan hakkı haline getirmektedir. Bugün bir insanın seyahat özgürlüğü engellenemeyeceği gibi, **internet erişimi de engellenemez.**

+ İnternet, sadece bir iletişim alanı değildir; o bir etkileşim alanıdır. Bu da interneti örgütlenme özgürlüğünün asli parçası haline getirmektedir. İnternet bugün insanların örgütlenmek, demokratik bir biçimde katılımda bulunmak, tepki ve protestolarını demokratik bir biçimde ifade etmek için kullandıkları en önemli platform haline gelmiştir. Bu yüzden internete erişim hakkı, **örgütlenme hakkının asli bir parçasıdır ve kısıtlanması demokratik hakların ihlali demektir.**

+ İnternetin gayri-merkezi, tarafsız, sınır-aşan ve etkileşimli doğası, onu düşünce, ifade, bilgi edinme ve haber alma özgürlüğünün asli parçası kılmaktadır. İnternete devlet denetimi ve gözetimi, ifade ve örgütlenme özgürlüğünün önündeki en büyük engellerdir. **Özgür ve sınırsız bir İnternet her kullanıcının hakkıdır.** İnternet erişim hakkının korunması, temel haklar olan düşünce, ifade, bilgi edinme ve haber alma özgürlüğünün korunmasına sıkı sıkıya bağlıdır. Dünyayı izleyebilen, kendi adına seçimler yapabilen geniş görüşlü fertler olabilmek için sınırsız ve özgür internet erişimi elzemdir.

+ Düzenleyici yasalar, **sansür ve yasakları değil, hak ve özgürlükleri** öncelemelidir. Suçla mücadele, çocuk ve aileyi korumak, terörizm gibi konjoktürel, muğlak, evrensel olmayan sebeplerle gerçekleştirilen erişim engellemeleri, kelime yasakları, merkezi filtrelemeler vb. yasak ve yaptırımlar sansürdür. İnternet'te sansür İnternet kullanıcılarının bilgiye erişim hak ve özgürlüğünü ihlal eder. **Sansürsüz İnternet **her yurttaşın hakkıdır.

+ İnsanlar şeffaf yasal zorunluluklar olmadığı sürece İnternet ortamındaki faaliyetleri nedeniyle kimliklerini açıklamaya zorlanamazlar. **Anonim olmak her kullanıcının hakkıdır.** Temel bir hak olan mahremiyet hakkı internet üzerinde yasal güvence altında olmak zorundadır.

+ İnternet kullanıcılarının kişisel verilerinin gizliliği esastır. Kullanıcılar, bu verilerinin hangi amaçlarla toplandığı ve nasıl kullanıldığını bilmek, buna itiraz etmek, kişisel verilerinin silinmesini, yok edilmesini istemek hakkına sahiptir.

 **[Elektrik Mühendisleri Odası] - [Alternatif Bilişim Derneği] - [İnternet Teknolojileri Derneği]**<br>
 **[Linux Kullanıcıları Derneği] - [SansüreSansür] - [Korsan Parti Hareketi]**<br>
 **[Tüm İnternet Evleri Derneği] - [Ankara Barosu Bilişim Kurulu] - [Engelli Web]** 

